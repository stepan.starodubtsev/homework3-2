let username = 'Stepan';
let password = 'secret';

let isCorrectPass = false;

while (!isCorrectPass) {
    let result = prompt(`Hello ${username}\n Enter your password:`);
    if (result === password) {
        alert("Password correct!");
        isCorrectPass = true;
    } else {
        alert("Password incorrect!\n Try again");
    }
}

let x = 5;
let y = 3;

alert(`${x}+${y}=${x + y}, 
${x}-${y}=${x - y},
${x}-${y}=${x - y},
${x}/${y}=${x / y}`);
